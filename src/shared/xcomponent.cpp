#include "shared/xcomponent.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <cstring>
#include <xcb/xcb.h>

using namespace px;
using namespace px::xutil;
using namespace std;

class XDisplayComponent::Priv {
public:
    Priv() {}
    Priv(const Priv&);
    Priv& operator=(const Priv&);
    bool invalid = false;
    string host = "";
    int display = 0;
    int screen = 0;
    string representation = "";
};

XDisplayComponent::XDisplayComponent(const char* dpy_str) {
    p = make_unique<Priv>();
    px::memory::UniquePointerWrapper_free<char> tmp_host;
    if (!xcb_parse_display(dpy_str, &tmp_host.pop(), &p->display, &p->screen)) {
        p->invalid = true;
    } else {
        p->host = tmp_host.get();
    }
}

XDisplayComponent::~XDisplayComponent() {}

XDisplayComponent::XDisplayComponent(const XDisplayComponent& other) {
    p = make_unique<Priv>(*other.p.get());
}

XDisplayComponent& XDisplayComponent::operator=(const XDisplayComponent& b) {
    if (&b != this) {
        p = make_unique<Priv>(*b.p.get());
    }
    return *this;
}

bool XDisplayComponent::operator==(const XDisplayComponent& other) const {
    return (p->host == other.p->host) && p->display == other.p->display && p->screen == other.p->screen;
}

bool XDisplayComponent::operator!=(const XDisplayComponent& other) const {
    return !(*this == other);
}

bool XDisplayComponent::operator<(const XDisplayComponent& other) const {
    int hostcmp = strcmp(p->host.c_str(), other.p->host.c_str());
    if (hostcmp < 0) {
        return true;
    } else if (hostcmp == 0) {
        if (p->display < other.p->display) {
            return true;
        } else if (p->display == other.p->display) {
            return p->screen < other.p->screen;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

bool XDisplayComponent::is_invalid() const {
    return p->invalid;
}

string XDisplayComponent::get_host() const {
    return p->host;
}

int XDisplayComponent::get_display() const {
    return p->display;
}

int XDisplayComponent::get_screen() const {
    return p->screen;
}

string XDisplayComponent::to_string() const {
    if (p->invalid) {
        return "";
    }
    if (p->representation == "") {
        p->representation = util::string_printf("%s:%d", p->host.c_str(), p->display);
        if (p->screen > 0) {
            p->representation += util::string_printf(".%d", p->screen);
        }
    }
    return p->representation;
}

XDisplayComponent::Priv::Priv(const Priv& other) {
    invalid = other.invalid;
    host = other.host;
    display = other.display;
    screen = other.screen;
}

XDisplayComponent::Priv& XDisplayComponent::Priv::operator=(const Priv& other) {
    if (this != &other) {
        invalid = other.invalid;
        host = other.host;
        display = other.display;
        screen = other.screen;
    }
    return *this;
}