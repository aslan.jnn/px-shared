/*
 * Helper for C++11's smart pointer.
 */
#pragma once

#include <cstdlib>
#include <memory>

namespace px {
    namespace memory {
        /**
         * Deletor for use with smart pointer, whose pointer was returned from "malloc". Should not be used with a
         * pointer to const.
         */
        struct free_delete {
            void operator()(void* x) {
                if (x != nullptr) {
                    free(x);
                }
            }
        };

        template<typename T>
        inline std::unique_ptr<T, free_delete> make_unique_malloc(T* ptr) {
            return std::unique_ptr<T, free_delete>(ptr);
        }

        template<typename T>
        inline std::shared_ptr<T> make_shared_malloc(T* ptr) {
            return std::shared_ptr<T>(ptr, free_delete{});
        }

        /**
         * @brief Wraps a pointer in a smart pointer.
         *
         * The pointer will be deleted upon this instance deletion, or when it's content is replaced with set(), or
         * when pop() is called.
         *
         * Note that this class doesn't check whenever a pointer has been deleted. It is caller responsibility not to
         * delete a pointer that is already / going to be wrapped in this wrapper.
         *
         * @tparam T Type of the data whose pointer will be wrapped.
         * @tparam deleter_type Deleter to use. (e.g.: free_delete or default_delete).
         */
        template<class T, class deleter_type>
        class UniquePointerWrapper {
        public:
            UniquePointerWrapper() {}

            UniquePointerWrapper(T* ptr) {
                bk_ptr.reset(ptr);
            }

            UniquePointerWrapper(const UniquePointerWrapper&) = delete;
            UniquePointerWrapper& operator=(const UniquePointerWrapper&) = delete;

            ~UniquePointerWrapper() {
                update();
            }

            /**
             * @brief Force update internal smart pointer.
             *
             * Should be called after a reference to internal pointer retreived from pop() was changed without
             * first calling pop(). Caller should call pop() before modifying the reference to the pointer instead of
             * calling this
             * after modifying it.
             */
            void update() {
                if (bk_ptr_raw != nullptr) {
                    bk_ptr.reset(bk_ptr_raw);
                    bk_ptr_raw = nullptr;
                }
            }

            void set(T* ptr) {
                update();
                bk_ptr.reset(ptr);
            }

            /**
             * @brief Get the pointer stored by this wrapper.
             *
             * @return Stored pointer value, or NULL if empty.
             */
            T* get() {
                update();
                return bk_ptr.get();
            }

            /**
             * @brief Clear the internal pointer value and return a reference to internal pointer.
             *
             * After call to this function, internal pointer value will be free-ed (deleted) and erased.
             * The reference returned by this function can be modified by caller. To ensure every pointer is deleted
             * (and avoiding memory leak),
             * caller should call this before modifying the reference.
             *
             * @return Reference to internal pointer storage. Its value is NULL.
             */
            T*& pop() {
                update();
                bk_ptr.reset();
                return bk_ptr_raw;
            }

        private:
            std::unique_ptr<T, deleter_type> bk_ptr;
            T* bk_ptr_raw = nullptr;
        };

        template<class T>
        using UniquePointerWrapper_free = UniquePointerWrapper<T, free_delete>;
    }
}