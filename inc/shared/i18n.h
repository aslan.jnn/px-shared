/**
 * Include to enable i18n and substitute (shorten) some variables.
 */
#pragma once

#include <libintl.h>
#define _(STRING) gettext(STRING)