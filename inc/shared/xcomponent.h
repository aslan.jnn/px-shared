#pragma once

#include <memory>
#include <string>

namespace px {
    namespace xutil {
        struct XDisplayComponent {
        public:
            XDisplayComponent(const char* dpy_str);
            ~XDisplayComponent();
            XDisplayComponent(const XDisplayComponent&);
            XDisplayComponent& operator=(const XDisplayComponent&);
            bool operator==(const XDisplayComponent& other) const;
            bool operator!=(const XDisplayComponent& other) const;
            bool operator<(const XDisplayComponent& other) const;
            bool is_invalid() const;
            std::string get_host() const;
            int get_display() const;
            int get_screen() const;
            std::string to_string() const;

        private:
            class Priv;
            std::unique_ptr<Priv> p;
        };
    }
}