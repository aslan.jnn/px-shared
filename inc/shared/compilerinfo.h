#pragma once

namespace px {
    /**
     * @brief Print some info about the compiler that compiles this library.
     * The detection is performed using known built-in macro from the compiler.
     */
    void print_compiler_info();
}