#pragma once

#include <functional>
#include <string>
#include <vector>

namespace px {
    namespace util {

        /**
         * A class that will call it's assigned function on deletion.
         */
        class DeleteInvoker {
        public:
            /**
             * Construct a DeleteInvoker with the specified function.
             * The specified function will be called on this DeleteInvoker's
             * deletion.
             * Please pay attention to the C++'s destruction order.
             * @param fn A void function with zero arguments.
             */
            DeleteInvoker(std::function<void(void)> fn);
            DeleteInvoker(const DeleteInvoker&) = delete;
            ~DeleteInvoker();
            DeleteInvoker& operator=(const DeleteInvoker&) = delete;

        private:
            std::function<void(void)> fn;
        };

        void fputsn(const char* const data, FILE* stream);
        std::string string_printf(const char* fmt, ...);
        std::string get_default_app_runtime_dir(std::string appname);
        std::string get_default_socketfile_path(std::string appname);
        std::string get_default_lockfile_path(std::string appname);
        std::string get_default_pid_path(std::string appname);

        template<typename STLMapType>
        bool check_and_get_mapped_value(STLMapType& map,
            typename STLMapType::key_type key,
            typename STLMapType::iterator& out) {
            auto it = map.find(key);
            if (it == map.end()) {
                return false;
            }
            out = it;
            return true;
        }

        template<typename STLMapType>
        bool check_and_get_mapped_value(std::vector<STLMapType*> maps,
            typename STLMapType::key_type key,
            typename STLMapType::iterator& out) {
            for (auto map : maps) {
                if (check_and_get_mapped_value(*map, key, out)) {
                    return true;
                }
            }
            return false;
        }
    }
}