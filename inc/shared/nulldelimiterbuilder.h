#pragma once

#include <memory>
#include <string>

namespace px {
    class NullDelimiterBuilder {
    public:
        NullDelimiterBuilder();
        NullDelimiterBuilder(int maxsize);
        /**
         * Construct a NullDelimiterBuilder containing just enough space to fit the supplied string.
         * @param sstr String to be put in the NullDelimiterBuilder.
         */
        NullDelimiterBuilder(std::string sstr);
        NullDelimiterBuilder(const NullDelimiterBuilder&) = delete;
        NullDelimiterBuilder(NullDelimiterBuilder&&);
        NullDelimiterBuilder& operator=(const NullDelimiterBuilder&) = delete;
        NullDelimiterBuilder& operator=(NullDelimiterBuilder&&);
        ~NullDelimiterBuilder();

        int add_token(const std::string& token);
        int add_token(const char* const token);
        /**
         * Get the total size of data stored in this buffer, including the trailing NULL.
         * @return The total size of data stored in this buffer.
         */
        int size();
        const char* const get_buffer();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}